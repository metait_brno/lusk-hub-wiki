# Process

Working at VR, we want to be as effective as possible. This means that although we love the freedom, there are some processes that we follow to make sure that everyone is on the same page.


## External Communication

If you are dealing with people outside the company, it is fine to use email. If you don't have one, we'll create it for you.


## Internal Communication

We're using **Slack**, where discussions are organized into the "channels", inside which are messages. Make sure that you have a look at our Using Slack guide before you get started.


## Project Management

We split everything into small chunks of work - [Milestones](../milestones/home.md) and track them in this Wiki. We use [Trello](https://trello.com/b/6RzyIOTc/hub-development) to split milestones into detailed list of tasks, responsible team as well as a realistic deadline.

### Milestone kick-off

Once milestone starts, it should have clearly defined goals, deliverables, responsible team as well as a realistic deadline. Milestones should not be long, usually 2 less weeks.

Before you start working on the task, make sure you:

- pick a card for you from the Trello backlog
- define deliverables for your task
- define set of sub-tasks related to the card
- estimate a realistic deadline (if it's not yet estimated)
- move your card into the *in progress* list


### Task Wrap-up

Once you reach your deadline, you should:

- review if you achieved your goals (if not, you should consider to split this task into a smaller ones or move to a different milestone if possible)
- move the card to the respective place - *Done* list (and [perform appropriate actions](./conventions/git.md), such as pushing your work to the remote server, creating merge request..)
- continue with another tasks within the current or future milestone

> If there's no appropriate task for you in the current milestone, you don't have to wait for the next milestone - there's much to do and you can contribute with other tasks in the Backlog for current/next milestone(s).

### Milestone Wrap-up

At the end of the milestone deadline, we summarize and check what was/wasn't done and create a new [release](../changelog/home.md). Once a new release is ready on our staging or production environment, we notify company about achieved milestone and released version.


### Take the Task out of the Milestone

This shouldn't become very often, but once it comes, you should consider with the team if its possible to take out the task and move it to a next milestone if it's not possible to finish it in the currently running one.


### Team Updates

Every day we take some time to get everyone on the same page. Team updates should be done every day, just before you start working. You should post them to the `standups-hub` Slack channel.

Before you post your update, make sure you put:

1. What have you worked on yesterday.
2. What have you planned for today.

For both your yesterday's and today's work you should highlight the following:

- You have encountered a major problem in your iteration.
- Any work that you have done to improve our processes.

An example of Team Update Post might look like this:

```
Stand-up dd. mm. YYYY

Achieved yesterday
- full integration api design
- started working on user profile settings

Today goals
- finish user profile settings
- fix file expiration issue on the CDN
```

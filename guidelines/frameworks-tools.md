# Frameworks & Tools & Libraries

Open-source makes our work easier - we use lot of open-source package. If it's possible, we're trying to push updates to some of them too.

Too keep dependency between all packages clear and strictly defined, we're using comprehensive **dependency management tool - [Composer](http://getcomposer.org)**.


## Which frameworks, libraries or tools are we using?

This is not a full list of them all, but these are probably the most important ones:

- **Nette** - Czech popular PHP framework is the engine of our applications.
- **Doctrine2** - modern ORM library for PHP. We're using integration to the Nette over the `kdyby/doctrine` package.
- **Restful** - expansive package for the Nette framework which allows us to write REST APIs faster. It's not developed by the community much anymore, but we're trying to keep it updated to work well.
- **Symfony Console** - we're using simple commands quite often and Symfony console is very useful and easy to use.
- **Codeception** - we're trying to automate our testing as much as possible. Codeception provides functionality for Unit tests, as well as for API or Selenium tests. 
- **OAuth2** - Open Auth Protocol is the core of our API security.


## About Security

Since our product is mostly about integration into existing platforms, the security is the key priority. To achieve this we're trying to use the **latest stable versions** of all libraries and frameworks, and having [all servers](../maintenance-support/infrastructure.md) updated as well.


---
### What next?

- [Technical Documentation](./technical-documentation.md)
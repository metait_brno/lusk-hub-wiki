# Getting Started

## Welcome on board!

We're really happy to having you on board to help us and contribute in many different ways. Our team is responsible for development of the [Integration Hub platform](../about/home.md).

The [team](../about/team.md) is relatively small. Definitely smaller than our expectations and goals, because we're working very hard to move the company to the top in this field. We're having young talented people there who's having a similar vision on company future.

**How it works there?**

- We don't have any complicated rules or procedures defined, we just have a couple of [general principles](./conventions.md), a [process definition](./process.md) to keep everybody in the loop.
- We don't check your hours, until you're delivering what you promised. Once the meetings are planned, it's a courtesy to be there. We use team combined calendar to share events within the company.
- Your suggestions are always welcome. It doesn't matter if it's about better project management, process definition or app quality.

**Welcome once again and have fun!**


---
### What next?

- [How to Use the Wiki](./conventions/wiki.md)
- [About](../about/home.md)
- [Process](process.md)
- [How to start with Development](./technical-docs/2016-01-07-how-to-start-with-development.md)
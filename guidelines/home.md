# Guidelines

## Are you new on board?

Did you read the [Getting Started](getting-started.md) document before? It's better place to start familiarize yourself with the processes and workflow, and you can find some interesting links there as well.


## Meet our Process

Working hard is our passion and the productivity is very important for us. Therefore some [process guidelines](process.md) exists.


## Frameworks, Tools, Conventions

Once you're familiarized with our [process](process.md), there are some [convention guidelines](conventions.md) you should know before you start.

Or just check out what [technologies, tools and frameworks](frameworks-tools.md) we use. There're lots of open-source libraries as well - maybe you already worked with some of them (or even invented some of them :-)).


---
### What next?

- [How to use the Wiki](./conventions/wiki.md)
- [Getting Started](./getting-started.md)
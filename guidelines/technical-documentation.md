# Technical Documentation

This is the place for storing all documents related to development, server`s setup and other documents which are too technical to be placed in different sections.

> Please sort them alphabetically within the right category.


## API reference

- [Full Integration API blueprint (Apiary)](http://docs.ihubintegration.apiary.io)


## General

- [How to run application locally](./technical-docs/2016-01-07-how-to-start-with-development.md)


## Media Conversion

- [Media Conversion Server Manual](./technical-docs/2016-01-09-media-conversion-server-manual.md)


## Server Setup

- [Installing AMS server](./technical-docs/2016-01-09-installing-ams-server.md)

# Media Conversion Server Manual

## Audio/Video Conversion

The tool [ffmpeg](http://www.ffmpeg.org/) is used for converting any audio and video files.

### Installation

All you need is to install ffmpeg package:

```
$ apt-get update
$ apt-get install ffmpeg
```

### Running

There is no need for additional setup.

### Example usage

```
$ ffmpeg -i input.vob output.mp4
```


## Image Conversion

We use [ImageMagick](http://www.imagemagick.org/script/index.php) for converting various formats of images.

### Installation

All you need is to install imagemagick package:

```
$ apt-get update
$ apt-get install imagemagick
```

### Running

There is no need for additional setup.

### Example usage

```
$ convert image.jpg image.png
```


## Document Conversion

We use [LibreOffice](https://www.libreoffice.org/) for converting various formats of documents.

### Installation

#### LibreOffice

First we need to install LibreOffice.

    $ apt-get update
    $ apt-get install libreoffice

Please ensure that you have a [Java Environment](https://packages.debian.org/default-jre) to achieve full functionality.

#### Unoconv

[Universal Office Converter](https://github.com/dagwieers/unoconv) (unoconv) is a command line tool to convert any document format that LibreOffice can import to any document format that LibreOffice can export.

You can either install it as a package (recommended way) or download it directly from [Github repository](https://github.com/dagwieers/unoconv).

    $ apt-get install unoconv

### Running

Unoconv works even when not running as a listener. But it has to start soffice service each time which is really slow. So prefered way is to run following command which starts soffice service permanently.

    $ unoconv --listener &

But even better is to have init script when in server mode.

Create a new file unoconv under /etc/init.d/.

    $ vim /etc/init.d/unoconv

And put following content inside the file.

    #!/bin/sh

    ### BEGIN INIT INFO
    # Provides:             unoconvd
    # Required-Start:       $network
    # Required-Stop:        $network
    # Default-Start:        2 3 4 5
    # Default-Stop:         0 1 6
    # Short-Description:    Document conversion by Universal Office Converter (unoconv)
    ### END INIT INFO

    case "$1" in
        start)
            /usr/bin/unoconv --listener &
            ;;
        stop)
            killall soffice.bin
            ;;
        restart)
            killall soffice.bin
            sleep 1
            /usr/bin/unoconv --listener &
            ;;
    esac

Change permissions for the file.

    $ chmod 755 /etc/init.d/unoconv

Update rc scripts.

    $ update-rc.d unoconv defaults

And now you can use the standart way to manage a service.

    $ service unoconvd start

### Example usage

    $ unoconv -f pdf input.docx

#### Supported formats

You can list supported formats by typing following command.

    $ unoconv --show

Output should be similar to following list (LibreOffice version 4.2.3.3):

    $ unoconv --show
    bib      - BibTeX [.bib]
    doc      - Microsoft Word 97/2000/XP [.doc]
    doc6     - Microsoft Word 6.0 [.doc]
    doc95    - Microsoft Word 95 [.doc]
    docbook  - DocBook [.xml]
    docx     - Microsoft Office Open XML [.docx]
    docx7    - Microsoft Office Open XML [.docx]
    fodt     - OpenDocument Text (Flat XML) [.fodt]
    html     - HTML Document (OpenOffice.org Writer) [.html]
    latex    - LaTeX 2e [.ltx]
    mediawiki - MediaWiki [.txt]
    odt      - ODF Text Document [.odt]
    ooxml    - Microsoft Office Open XML [.xml]
    ott      - Open Document Text [.ott]
    pdb      - AportisDoc (Palm) [.pdb]
    pdf      - Portable Document Format [.pdf]
    psw      - Pocket Word [.psw]
    rtf      - Rich Text Format [.rtf]
    sdw      - StarWriter 5.0 [.sdw]
    sdw4     - StarWriter 4.0 [.sdw]
    sdw3     - StarWriter 3.0 [.sdw]
    stw      - Open Office.org 1.0 Text Document Template [.stw]
    sxw      - Open Office.org 1.0 Text Document [.sxw]
    text     - Text Encoded [.txt]
    txt      - Text [.txt]
    uot      - Unified Office Format text [.uot]
    vor      - StarWriter 5.0 Template [.vor]
    vor4     - StarWriter 4.0 Template [.vor]
    vor3     - StarWriter 3.0 Template [.vor]
    wps      - Microsoft Works [.wps]
    xhtml    - XHTML Document [.html]

    The following list of graphics formats are currently available:

    bmp      - Windows Bitmap [.bmp]
    emf      - Enhanced Metafile [.emf]
    eps      - Encapsulated PostScript [.eps]
    fodg     - OpenDocument Drawing (Flat XML) [.fodg]
    gif      - Graphics Interchange Format [.gif]
    html     - HTML Document (OpenOffice.org Draw) [.html]
    jpg      - Joint Photographic Experts Group [.jpg]
    met      - OS/2 Metafile [.met]
    odd      - OpenDocument Drawing [.odd]
    otg      - OpenDocument Drawing Template [.otg]
    pbm      - Portable Bitmap [.pbm]
    pct      - Mac Pict [.pct]
    pdf      - Portable Document Format [.pdf]
    pgm      - Portable Graymap [.pgm]
    png      - Portable Network Graphic [.png]
    ppm      - Portable Pixelmap [.ppm]
    ras      - Sun Raster Image [.ras]
    std      - OpenOffice.org 1.0 Drawing Template [.std]
    svg      - Scalable Vector Graphics [.svg]
    svm      - StarView Metafile [.svm]
    swf      - Macromedia Flash (SWF) [.swf]
    sxd      - OpenOffice.org 1.0 Drawing [.sxd]
    sxd3     - StarDraw 3.0 [.sxd]
    sxd5     - StarDraw 5.0 [.sxd]
    sxw      - StarOffice XML (Draw) [.sxw]
    tiff     - Tagged Image File Format [.tiff]
    vor      - StarDraw 5.0 Template [.vor]
    vor3     - StarDraw 3.0 Template [.vor]
    wmf      - Windows Metafile [.wmf]
    xhtml    - XHTML [.xhtml]
    xpm      - X PixMap [.xpm]

    The following list of presentation formats are currently available:

    bmp      - Windows Bitmap [.bmp]
    emf      - Enhanced Metafile [.emf]
    eps      - Encapsulated PostScript [.eps]
    fodp     - OpenDocument Presentation (Flat XML) [.fodp]
    gif      - Graphics Interchange Format [.gif]
    html     - HTML Document (OpenOffice.org Impress) [.html]
    jpg      - Joint Photographic Experts Group [.jpg]
    met      - OS/2 Metafile [.met]
    odg      - ODF Drawing (Impress) [.odg]
    odp      - ODF Presentation [.odp]
    otp      - ODF Presentation Template [.otp]
    pbm      - Portable Bitmap [.pbm]
    pct      - Mac Pict [.pct]
    pdf      - Portable Document Format [.pdf]
    pgm      - Portable Graymap [.pgm]
    png      - Portable Network Graphic [.png]
    potm     - Microsoft PowerPoint 2007/2010 XML Template [.potm]
    pot      - Microsoft PowerPoint 97/2000/XP Template [.pot]
    ppm      - Portable Pixelmap [.ppm]
    pptx     - Microsoft PowerPoint 2007/2010 XML [.pptx]
    pps      - Microsoft PowerPoint 97/2000/XP (Autoplay) [.pps]
    ppt      - Microsoft PowerPoint 97/2000/XP [.ppt]
    pwp      - PlaceWare [.pwp]
    ras      - Sun Raster Image [.ras]
    sda      - StarDraw 5.0 (OpenOffice.org Impress) [.sda]
    sdd      - StarImpress 5.0 [.sdd]
    sdd3     - StarDraw 3.0 (OpenOffice.org Impress) [.sdd]
    sdd4     - StarImpress 4.0 [.sdd]
    sxd      - OpenOffice.org 1.0 Drawing (OpenOffice.org Impress) [.sxd]
    sti      - OpenOffice.org 1.0 Presentation Template [.sti]
    svg      - Scalable Vector Graphics [.svg]
    svm      - StarView Metafile [.svm]
    swf      - Macromedia Flash (SWF) [.swf]
    sxi      - OpenOffice.org 1.0 Presentation [.sxi]
    tiff     - Tagged Image File Format [.tiff]
    uop      - Unified Office Format presentation [.uop]
    vor      - StarImpress 5.0 Template [.vor]
    vor3     - StarDraw 3.0 Template (OpenOffice.org Impress) [.vor]
    vor4     - StarImpress 4.0 Template [.vor]
    vor5     - StarDraw 5.0 Template (OpenOffice.org Impress) [.vor]
    wmf      - Windows Metafile [.wmf]
    xhtml    - XHTML [.xml]
    xpm      - X PixMap [.xpm]

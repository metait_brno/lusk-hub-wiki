# Installing AMS server

## Centos verze 6.X (Final)

Before installation of AMS is recommended to update Centos distribution on server.

```
cat /etc/*release*
yum update
```

## Installation for Adobe Media Server

- [Download](http://www.adobe.com) latest Adobe Media Server package
- Unzip locally all files provided by Adobe and copy content of folder 'linux' to target server.
- Connect via SSH to the server and navigate to the folder where is files for installation. Proceed with following commands:
```
tar -xzf AdobeMediaServer_5_LS1_linux64.tar.gz
cd AMS_5_0_3_r3029/
./installAMS
```

- Please follow installation instructions. When asked enter AMS Serial Number:
```
1650-5006-6267-9816-2502-3754
```

- Keep default value for target instalation directory `/opt/adobe/ams`
- Enter administrative username `administrator`
- Enter and confirm administrative password `h8SE6rEyoRbZ`
- Keep default settings for system user/group `ams`
- Install apache and keep it to listen port `80`
- Keep autodetect option for detecting IP address
- Keep all other setting on default values
- Confirm installation start - **y**
- When AMS installation is completed, please remove installation files uploaded to the server.


## Check the state of the server

```
cd /opt/adobe/ams/
./amsmgr list
```

Should display list of running AMS services:

```
Adobe Media Server services:
ams

Adobe Media Server running services:
Processes for service "ams" in directory: /opt/adobe/ams

PID      TTY        TIME       CMD
14650    pts/0      00:00:00   amsmaster

PID      TTY        TIME       CMD
14777    pts/0      00:00:00   amsadmin
```


## AMS configuration

Configure `ams.ini` and `Stream.xml` for proper FLV Cache and user rights settings.

- Edit `/opt/adobe/ams/conf/Server.xml`
	- Change value for node `<Mask>` from value `017` to `002`
- Edit `/opt/adobe/ams/conf/ams.ini`
	- Change value for parameter `SERVER.FLVCACHE_MAXSIZE` from default `500` to `2000`
- Create new AMS Application folder:

For new AMS Application folder u can use this bash commands:

```
cd /opt/adobe/ams/applications/
mkdir [application_name]
mkdir [application_name]/streams
chown -R ams.ams [application_name]
chmod -R 0775 [application_name]
```

Create application SymLink for `webroot` folder, to enable **http streaming**:

```
cd /opt/adobe/ams/webroot
ln -s /opt/adobe/ams/applications/[application_name]/streams/_definst_/ [application_name]
```

Configure `/opt/adobe/ams/conf/Logger.xml` for proper server logging. For almost all logger section is recommended to have this setting:

```
<Directory>${LOGGER.LOGDIR}</Directory>
<FileName>access.[DDMMYYYY].log</FileName>
<Time></Time>
<Rotation>
  <MaxSize>102400</MaxSize>
  <Schedule type="daily">00:00</Schedule>
  <History>999</History>
  <Rename>true</Rename>
</Rotation>
<Events>*</Events>
<Fields>*</Fields>
```

Create `Application.xml` and `main.asc` files from templates in a folder `[application_name]/`.


## Basic AMS server commands

### Start AMS server

```
cd /opt/adobe/ams/
./server start
./adminserver start
```

### Stop AMS server

```
cd /opt/adobe/ams/
./server stop
./adminserver stop
```


## Server configuration:

### Enable ports

It's necessary to open all required ports for AMS and Apache communication:

- 80 (Apache)
- 1935 (RTMP)
- 1111 (AMS Administration)
- 8080 (HTTP Tunnel)

```
# iptables -L -n
# iptables -A INPUT -m state --state NEW -p tcp --dport 8080 -j ACCEPT
# iptables -I INPUT 1 -i eth0 -p tcp --dport 80 -m state --state NEW,ESTABLISHED -j ACCEPT
# iptables -I INPUT 2 -i eth0 -p tcp --dport 1935 -m state --state NEW,ESTABLISHED -j ACCEPT
# iptables -I INPUT 3 -i eth0 -p tcp --dport 1111 -m state --state NEW,ESTABLISHED -j ACCEPT
# iptables -I INPUT 4 -i eth0 -p tcp --dport 8080 -m state --state NEW,ESTABLISHED -j ACCEPT
# service iptables save
```

### Create new SSH keys to authorize communication with other servers

```
cd /root/.ssh/
ssh-keygen -t rsa
cat id_rsa.pub
```
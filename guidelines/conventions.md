# Conventions

Conventions are a set of guidelines for a specific type of language/tool.

In the iHub, and VR as well, we're thinking, that having standardized output is very helpful. It saves time to everybody - it's better understandable for a new in-comers, and also for masters and project owners for code reviews. Process can be automated then and allows to use continuous integration principles and other advantages.

These applied for programming language as well as for general documents.


## How to manage content in the Wiki?

This is the first guideline you should be interested in - it's very important to know the structure of this Wiki, to be able to find any document or article you need for the next steps.

[Take me to a Wiki Guidelines](conventions/wiki.md).


## Another tours:

- [How to use Git](./conventions/git.md)
- [PHP Coding Standards](./conventions/php.md)


---
## What next?

- [Frameworks & Tools](./frameworks-tools.md)
# How to use Git

Git really saves our time and lifes, and allows us to be as productive as possible. It's a very powerful tool and automates our daily work. For our private code we're using self-hosted git management - [GitLab Community Edition](https://gitlab.com).


## About GitLab CE

[GitLab CE](https://gitlab.com) is a self-hosted, scalable, open source package. It's full of features: repository management, code reviews, issue tracking, continuous integration and delivery, and much more.

It's simple to use and it's workflow is very similar to [GitHub](http://www.github.com).

Our GitLab instance is available at [gitlab.video-recruit.com](https://gitlab.video-recruit.com) (if you don't have access yet, write to a [suitable person](../../maintenance-support/who-is-who.md)).


## How do we work with GitLab?

- we're trying to split iHub into small chunks of sub-apps/libraries. Apps are running as a standalone hosts, otherwise libraries are shared across more apps over the [composer](https://getcomposer.org) package manager.
- every single application/library sits in it's own repository
- our **apps** are running on **[multiple environments](../../maintenance-support/infrastructure.md)** (local, dev, staging, production) and therefore you always find master, staging, production branches in the application repository
- both apps and libraries are regularly tagged and released - we don't forcing to rewrite the git history
- we're **building, testing and deploying** over the GitLab's continuous integration and deployment tool
- you don't have to remember all **weak places** - just use [GitLab's Issues](https://gitlab.com/gitlab-org/gitlab-ce/issues) to keep the focus for future
- we're doing **code reviews** by using [GitLab's Merge Requests](https://gitlab.com/gitlab-org/gitlab-ce/merge_requests)


## What's the typical Workflow?

- before you start to work on some task, just **pull a master** and create a new branch from it for this task - check our branching conventions below
- **split** your work into **more logical commits** - do not finish whole task by 1 commit if it's not the necessary case
- **push** your branch regularly to the remote
- try to avoid [BC breaks](https://en.wikipedia.org/wiki/Backward_compatibility)
- try to **avoid non-fast-forward merging** - merge commit messages are breaking the readability of the history
- try to **test** everything as much as possible once you're done (since we're not writing regular automated tests yet, try to test all functionality related to your changes)
- once you're done with testing, **push** fixes and all not-yet-pushed changes to a remote
- create a **merge request** from your branch to the **master** - check our naming convention below
- you can **delete** your local branch now and wait until it's successfully merged (if it's not merged and needs some updates, you can check the branch out again once you need it)

> It's simple: pull -> split -> commit -> test -> push -> merge request -> clean-up


## Branch Conventions

- use **reasonable title**, use **hyphens** instead of spaces
- differentiate your branches by adding a significant prefix: `feature-`, `improvement-`, `bugfix-`, `hotfix-`, `refactoring-`


## Merge Request Conventions

- use **reasonable title**
- differentiate **work in progress** merge requests by **prepending** `[WIP]` before the title
- remove `[WIP]` once you're done with the task - it allows us to merge your work
 

## Writing a meaningful Commit Message

We're trying to keep git history **well readable** - a meaningful message is **a must**. Therefore we're having conventions for commit messages:

- **differentiate** your commits - **prepend the type** characters before the message (it allows to find out all fixes very quickly just from log), the allowed list of types is following:
    - `[-]` bug-fix
    - `[!]` hot-fix (for fixes on the production)
    - `[+]` feature
    - `[*]` improvement
    - `[~]` deprecated
    - `[#]` re-factoring
- try to describe your commits in **short sentence** (about 160 characters long):
```
[-] Coding Standards fix - missing EOL@EOF.
```
- if you want to include **more detailed description**, use semicolon at the end of the message and append the list of points below (add empty line between the message and the list):
```
[-] Coding Standards fix:

 - missing EOL@EOF
 - invalid indentation
 - using tabs inastead of spaces
```


---
### What next?

- [Coding Standards for PHP](php.md)
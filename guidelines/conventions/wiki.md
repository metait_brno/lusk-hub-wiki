# How to Use the Wiki

The wiki is used as the single source of truth for everything about our product. Whenever a new decision is reached, it needs to be written at the right spot in the wiki. If you are adding a new file, please sort it carefully into the existing structure.


## Structure

- **Introduction**
- **About** - Everything you should know about the Team and the Product.
- **Meeting Minutes** - Articles, notes for every current, future and past meeting. Name of the article should contain the date in `YYYY-MM-DD` format. Every article must contain `list of presented users, date, list of discussed`. chapters
- **Milestones (Roadmap)** - List of articles. Each article represents the milestone definition and has to contain `number, due date and description`. You can use article template *(_templates/milestone.md)* and extends it's blocks.
- **Guidelines**
    - **Getting Started** - This chapter is just for new members in the team. You find there introduction to our work-flow, processes, guidelines and some more useful links.
    - **Process** - How are we working? Internal and external communication, project management - everything on one place.
    - **Conventions** - Well formed and readable code is our priority. Therefore code conventions exists.
    - **Frameworks & Tools** - Do you know which tools are important for us? You should know that.
    - **Technical Documentation** - The right place where you'll find important articles and How-to for app/server setup. You can find the generated apidoc there too. Name of the article should contain the date in `YYYY-MM-DD` format.
- **Work in Progress** - All documents related to the current work. File names should contain the current date, and there should be also information about milestone or task which is the article related to.
- **Release History (Changelog)** - Once we deploy a new version out to the public servers, you'll find the list of changes there. This includes changelog for all apps.
- **Maintenance & Support**
    - **Infrastructure** - How many servers are we currently running? From which supplier? Where I can find error logs? What is the current state of all servers?
    - **Deploy Guide** - There's a new release ready - how to deploy it to a dev or staging environment? How to run app in case its down?
    - **Technical Support** - Everything you should know, when there's a bug or a new candidate/client support request. Who should be contacted, what to do or who should be contacted in case app is not running?
    - **Accounts** - Domains, DNS and mail management, suppliers administration like Mandrill, OVH. 3rd party libraries and accesses to them.
    - **Who's Who** - List of departments and responsible persons.


## How-to write new documents?

- use English
- use Markdown
- put the docs to the right place in the structure
- update the docs once the content is not actual
- make sure your docs are easy to read even for non-programmers
- name your files lower-cased, separate words with hyphens, and if you're including the date in the file name, use `YYYY-MM-DD` and put it at the beginning of the name
- fix broken links if you find some


---
### What next?

- [How to use Git](git.md)
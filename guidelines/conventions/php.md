# Coding Standards for PHP

We follow the recommendations defined in the [PSR-1](http://www.php-fig.org/psr/psr-1/) and [PSR-2](http://www.php-fig.org/psr/psr-2/) documents with [two differencies](http://www.php-fg.org/pgs-2/):

- tabs are used for indenting
- PHP constants TRUE, FALSE, and NULL are in upper case


## General rules
- 1 tab is used for indenting instead of spaces. Tabs are allowed only at the begin of line.
- The recommended line length limit is 120 characters, lines should be 80 characters or less long.
- Only UTF-8 without BOM is allowed.
- Code must always be opened by the full-form PHP tag: `<?php`
- The closing `?>` tag must be omitted from files containing only PHP.
- There should not be more than one statement per line.
- No trailing whitespace at the end of lines.
- All files must end with a single blank line.
- File name should match class name if possible.
- Any file that contains any PHP code must end with the extension `.php`, or `.phpt` in case of PHP tests.
- Defining constants or functions in global scope is not allowed.
- Do not keep unused code in comments, remove unsued `use` statements - keep it clean.

## Basic expressions
### Strings
- When a string is literal (contains no variable substitutions), the single quote should be used to demarcate the string, except when a literal string itself contains apostrophes, it is recommended to demarcate the string with double quotes. This rule is applied for the string variables and all argument values, array keys - everywhere where quoted string is used.
- Strings may be concatenated using the `.` operator. A space must always be added before and after the `.` operator to improve readability. It is permitted to break the statement into multiple lines:

```
$sql = 'SELECT `id`, `name` FROM `people`'
    . 'WHERE `name` = ?'
    . 'ORDER BY `name`';
```

with variable substitution:

```
$sql = "SELECT `id`, `name` FROM `$table`"
    . "WHERE `name` = ?"
    . "ORDER BY `name`";
```

### Arrays
- Trailing space must be added after each comma delimiter to improve readability.

```
$sampleArray = [1, 2, 3, 'test'];
```

- When declaring associative arrays, it is encouraged to put each key and value pair on separated line, indented by 1 tab. After last pair the comma must be added.

```
$sampleArray = [
	'firstKey'  => 'firstValue',
	'secondKey' => 'secondValue',
];
```

### Keywords
- PHP keywords must be in lower case.
- The PHP constants `TRUE`, `FALSE`, and `NULL` must be in upper case.

### Method and function calls
- There must not be a space between the method or function name, the opening parenthesis, after the opening parenthesis and before the closing parenthesis.
- In the argument list, there must be one space after each comma, and there must no be a space before each comma.

```
bar();
$foo->bar($arg1);
Foo::bar($arg1, $arg2);
```

For functions whose arguments permitted arrays, the function call may include array and can be split into multiple lines to improve readability. In these cases, the standards for writing arrays still apply:

```
fooBar(array(1, 2, 3), 2, 3);

$foo->bar(1, 2, array(
    'firstKey'  => 'firstValue',
    'secondKey' => 'secondValue',
), 3, 4);
```

Argument lists may be split across multiple lines, where each subsequent line is indented once. When doing so, the first item in the list must be on the next line, and there must be only one argument per line:

```
$foo->bar(
    $arg,
    $arg2,
    $arg3
);
```

## Classes
- The `extends` and `implements` keywords must be declared on the same line as the class name.
- Any code within a class must be indented by 1 tab.
- The opening brace for the class must be on the line underneath the class name, the closing brace for the class must go on the next line after the body.
- Interface classes must follow the same conventions as other classes, however should begin with `I` letter.
- Class members implementation should follow this order as it is practical:
	1. Member Variables
		1. public
		2. protected
		3. private
	2. Constructor, destructor
	4. Methods
		1. public
		2. protected
		3. private

### Class properties
- Properties must be declared in `camelCase`.
- The `var` keyword must not be used to declare a property.
- Property names must not be prefixed with a single underscore to indicate protected or private visibility.
- Properties must describe an entity not the type or size.
- Properties must have documentation block with @var directive and specified type.

### Constants
- They must always have all letters capitalized, separated by underscore characters.

### Methods
- They must be declared in `camelCase`.
- Visibility must be declared on all methods (with exception of interface declarations).
- Method names must not be prefixed with a single underscore to indicate protected or private visibility.
- Like classes, the brace is always written on the line underneath the function name. There is no space between the function name and the opening parenthesis for the arguments. All arguments should have type hint if possible.
- Functions and methods must have documentation block with specified types of parameters and type of returned value (if method is void only, return type of void should be specified).
- If method returns boolean, try to prefix name with “is”, “can”, “has” or similar meaningful prefix.
- The return value must not be enclosed in parentheses.
- When present, the `abstract` and `final` declarations must precede the visibility declaration.
- When present, the `static` declaration must come after the visibility declaration.

## Control Statements
- There must be one space after the control structure keyword.
- There must not be a space after the opening parenthesis and before the closing parenthesis.
- There must be one space between the closing parenthesis and the opening brace.
- The structure body must be indented once.
- The closing brace must be on the next line after the body.

### if, elseif, else
- Comparison “strong typed” operators (=== and !==) are preferred before “weak typed” ones (== and !=).
- If weak typed comparison operator is used, the intention must be documented with a comment.
- The keyword elseif must be used instead of else if.

```
if ($a === 2) {
    $x = 2;

} elseif ($a == 0) { // intentionally ==, $a may be NULL
    $x = 4;

} else {
    $x = 7;
}
```

### switch, case
- There must be a comment when fall-through is intentional in a non-empty case body.

```
switch ($numPeople) {
    case 1:
        echo 'First case, with a break';
        break;

    case 2:
        echo 'Second case, which falls through';
        // break intentionally omitted

    default:
        break;
}
```

### while, do while
```
while ($expr) {
    // structure body
}
```

```
do {
    // structure body;
} while ($expr);
```

### for
```
for ($i = 0; $i < 10; $i++) {
    // for body
}
```

### foreach
```
foreach ($iterable as $key => $value) {
    // foreach body
}
```

### try, catch
```
try {
    // try body
} catch (FirstExceptionType $e) {
    // catch body
} catch (OtherExceptionType $e) {
    // catch body
}
```


---
### What next?

- [Frameworks & Tools](../frameworks-tools.md)
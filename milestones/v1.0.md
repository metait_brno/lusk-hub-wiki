{% extends "../_templates/milestone.md" %}

{% block release %}1.0{% endblock %}

{% block date %}4. 3. 2016{% endblock %}

{% block tasks %}
- dates refactoring
- extending admin section
	- general settings
	- general VI settings
	- partner's VI settings
	- partner's TOC management
	- client's profile/languages
- partner specific Terms & Conditions within the WebRecorder
- partner specific Email templates (used as templates for created clients)
- ~~video-interview playback improvement for partially recorded interviews~~ [[v1.1](./v1.1.md)]
{% endblock %}

{% block fixes %}
- implement authentication over HTTP header to the CRON
- video-interview autoplay
- ~~set questions draggable issue~~ [[v1.1](./v1.1.md)]
{% endblock %}
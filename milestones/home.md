# Milestones

> If you're interested into the longterm overview, [check our Roadmap](./roadmap/index.html).

The Roadmap is split into smaller iterations - milestones. You can find the whole list there:

- ~~[v0.11](v0.11.md)~~
- ~~[v0.12](v0.12.md)~~
- ~~[v0.13](v0.13.md)~~
- ~~[v0.14](v0.14.md)~~
- [v1.0](v1.0.md) (*previously known as v0.15*)
- [v1.1](v1.1.md)
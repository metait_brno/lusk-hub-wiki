# What is Video Recruit?

Video Recruit is an end-to-end hiring solution that lets recruiters find 5* candidates quickly and efficiently.

Video Recruit is currently split into 2 teams: [**Lusk Team**](http://www.lusk.io), responsible for the development of the own ATS platform and **iHub Team**, responsible for the development of the integration platform and HR tools like a video-interviews, live interviews and more.

> If you're new there, follow to the [Getting Started](./guidelines/getting-started.md) chapter.


## About the Integration Hub Platform

Integration Hub Platform was developed to offer a new way how to sell our tools to our partners. From the beginning it offers 2 ways, how it can be integrated to partner systems: full integration over API and usage of the iHub User interface.

If you want to read more about the iHub platform and it's team, continue to the respective chapter - [About](about/home.md).

During the development, we are opened to the new ideas. We love freedom, but follow some integrated [processes](./guidelines/process.md) and [internal guidelines](./guidelines/home.md).


## Continous Development

We build very fast, and try to make the app history open to everybody. Therefore we develop in time cycles defined by a [Milestone](./milestones/home.md) and [releasing](./changelog/home.md) as often as possible at the end of each milestone.


---
### What next?

- [How to Use the Wiki](./guidelines/conventions/wiki.md)
- [About](./about/home.md)
- [Process](./guidelines/process.md)
- [Getting Started](./guidelines/getting-started.md)
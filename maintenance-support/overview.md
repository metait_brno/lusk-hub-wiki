{% set gitlab = "https://gitlab.video-recruit.com" %}
# Overview



## Primary Services


### [GitLab]({{ gitlab }}/)

GitLab serves as single source of truth.
Its CI is running builds, tests, pusing to docker registries and deploying environments.

GitLab users are used for authentication and authorization to docker registries. They include people and virtual users for CI runners and Rancher environments.

GitLab projects (repositories) are used as build source for docker image(s) or Rancher environment definitions. Environment definitions use [Secret Variables](http://docs.gitlab.com/ce/ci/variables/README.html) for specific configuration. Environment definitions can be mapped to single Rancher environment.


### [Rancher](env/orchestration.md)

Rancher orchestrates environments.

To access environment in Rancher you must have GitHub account and authorization to access the environment.

Environment consists of stacks of services running on servers linked by single secure network that can span accross datacenters. Environment uses docker images from docker registries to run services. GitLab project secret variables and passed during a deployment for environment specific configuration and settings.


## Secondary Services


### Docker Registries
Docker registry is a storage for docker images.

Access registry using GitLab username and private token[^1] as a password. You must have access to the GitLab project either via group or membership to pull image. Owner or master can push image[^2].

Note: Prefix `docker-` in projects names is optional ommited.
ie.: You can access project named `invire/docker-gitlab-ci-runner` using
```
docker pull ci.invire.me:5000/invire/gitlab-ci-runnner
```
Note: Login ahead of pulling or pushing image to avoid unauthorized error.
```
docker login reg.invire.me
```

#### CI registry `ci.invire.me:9000`

Storage for images for CI builds. That way they are available in next CI steps like tests and publishing images to other registries[^3].

#### Staging registry `ci.invire.me:5000`

Storage for images of feature branches that passed tests.

#### Production registry `reg.invire.me`

Storage for production ready images. Images must have [SEMVER 2.0.0](http://semver.org/spec/v2.0.0.html) tag. Ideally all production environments should be running only images from production registry.


### GitLab CI Runners

All runners are running [invire/docker-gitlab-ci-runner]({{ gitlab }}/invire/docker-gitlab-ci-runner) which is a runner with custom scripts and logic for (un)assigning to GitLab projects.

GitLab selects runner for tasks by its assigned projects and matching tags.

#### Generic

Generic runners services are defined in [invire/environment-ci]({{ gitlab }}/invire/environment-ci/blob/master/group-ihub/docker-compose.yml) in group-ihub stack.

All runners are running steps in shell and have access to DinD (Docker in Docker) so they can run docker and docker-compose commands.

Also contains custom scripts for building and managing images that way `.gitlab-ci.yml` file can stay rather small and simple. If in doubt use [ihub/app](https://gitlab.video-recruit.com/ihub/app/blob/master/.gitlab-ci.yml) project as a generic referrence and [invire/docker-backup](https://gitlab.video-recruit.com/invire/docker-backup/blob/master/.gitlab-ci.yml) or [ihub/cdn](https://gitlab.video-recruit.com/ihub/cdn/blob/master/.gitlab-ci.yml) on how to write tests.

#### Deployers

Deployers deploy environments. To avoid keeping Rancher environment access keys for all environments in single repository (available to all services and runners) there is one deployer per environment. Their repository names follow `invire/environment-ci-<team>-<env>-deployer` pattern.


[^1]: You can find your private token in GitLab under your [Profile > Account]({{ gitlab }}/profile/account).
[^2]: CI runners have their rights specified in [invire/registry-gitlab-oauth]({{ gitlab }}/invire/registry-auth-gitlab) in file access.go.
[^3]: Publishing in this context means pulling from CI registry and push to another registry.

# DNS


## Record Types
 Each server must have inventory record, reverse DNS record and designation record.  
**Services must use application or round-robin records when referencing servers.**

### Inventory record
 Unique identifier for server.

```dns
<name[unique]>.<team>.invire.me. A 1.2.3.4
```

Name must be unique in team.  
Name should be unique in team history.

### Reverse record
 Used for looking up of inventory name by IP.

Must be opposite to inventory record.

### Designation record[^1]

```dns
<designation[counter:2]>.<city>.<provider>.<environment>.<team>.invire.me. CNAME <#inventory record>
```

Must point to inventory record.  
Designation counter is per city left zero padded to width of 2.  
One server should not be designated for more then one tasks.

### [Round-robin DNS](https://en.wikipedia.org/wiki/Round-robin_DNS) records
 Used for distributing load to designated servers like web.

```dns
<designation>.<environment>.<team>.invire.me. A 1.2.3.4
<designation>.<city>.<environment>.<team>.invire.me. A 1.2.3.4
```

### Service records

```dns
*.example.com. CNAME <#round-robin dns|inventory>
<service>.example.com. CNAME <#round-robin dns|inventory>
```

Must point to Round-robin DNS record or inventory record.

### Application records

```dns
<designation[counter:2]>.example.com. CNAME <#inventory record>
```

Must point to inventory record.  
Designation counter is per domain left zero padded to width of 2.  

## Terms

### Name
 Item from mnemonic project[^2] **unique for team** and **reserved forever**.

[source](names.txt)

{% include "./terms.md" %}

## Example

Example setup for iHub production environment.

Two web servers, two ams servers (different specializations) and one worker server. Single data center SRB-1 and single provider OVH.

### Inventory

| name | ip | designation | specialization | provider | instance | data center |
| --- | --- | --- | --- | --- | --- | --- |
| fiona | 1.2.3.4 | web | | OVH | VPS 3 | SBG-1 |
| armada | 1.2.3.5 | web | | OVH | VPS 3 | SBG-1 |
| monitor | 1.2.3.6 | ams | video-interview | OVH | VPS 1 | SBG-1 |
| flash | 1.2.3.7 | ams | introduction-video | OVH | VPS 1 | SBG-1 |
| scuba | 1.2.3.8 | wrk | | OVH | VPS 1 | SBG-1 |

### DNS Records

```dns
; Application records
*.video-recruit.io.         CNAME web.video-recruit.io.
admin.video-recruit.io.     CNAME web.video-recruit.io.
api.video-recruit.io.       CNAME web.video-recruit.io.
interview.video-recruit.io. CNAME web.video-recruit.io.
logger.video-recruit.io.    CNAME web.video-recruit.io.

; Application records
ams01.video-recruit.io. CNAME monitor.ihub.invire.me.
ams02.video-recruit.io. CNAME flash.ihub.invire.me.

; Round-robin DNS
web.video-recruit.io. A 1.2.3.4 ; fiona.ihub.invire.me.
web.video-recruit.io. A 1.2.3.5 ; armada.ihub.invire.me.

; Designation records
web01.sxb.ovh.pro.ihub.invire.me. CNAME fiona.ihub.invire.me.
web02.sxb.ovh.pro.ihub.invire.me. CNAME armada.ihub.invire.me.
ams01.sxb.ovh.pro.ihub.invire.me. CNAME monitor.ihub.invire.me.
ams02.sxb.ovh.pro.ihub.invire.me. CNAME flash.ihub.invire.me.
wrk01.sxb.ovh.pro.ihub.invire.me. CNAME scuba.ihub.invire.me.

; Inventory records
fiona.ihub.invire.me.   A 1.2.3.4
armada.ihub.invire.me.  A 1.2.3.5
monitor.ihub.invire.me. A 1.2.3.6
flash.ihub.invire.me.   A 1.2.3.7
scuba.ihub.invire.me.   A 1.2.3.8
```


[^1]: [MNX.io: A Proper Server Naming Scheme](http://mnx.io/blog/a-proper-server-naming-scheme/)  
[^2]: [mnemonics project](http://web.archive.org/web/20090918202746/http://tothink.com/mnemonic/wordlist.html)

### City
 Three characters without country from [UN/LOCODE](http://www.unece.org/cefact/locode/service/location.html) describing **physical location of server**.

Example: [France Roubaix](http://www.unece.org/fileadmin/DAM/cefact/locode/fr.htm) 🠊 FR ROU 🠊 ROU

| term | UN/LOCODE | name | country |
| --- | --- | --- | --- |
| ROU | FR ROU | Roubaix | France |
| SXB | FR SXB | Strasbourg | France |

### Data Center

| term | provider | city | |
| --- | --- | --- | --- |
| SBG-1 | OVH | SXB | [status](http://status.ovh.com/vms/index_sbg1.html) |

### Designation
 Does not have to be a meaningful shortcut.

| term | label | description |
| --- | --- | --- |
| ams | adobe-media-server | Adobe Media Server |
| cid | ci-cd | CI & CD **pet server** |
| dns | | Name server |
| log | | Logging |
| mon | monitoring | Monitoring **pet server** |
| rnc | rancher | Rancher server |
| sql | | Database server |
| sto | | Storage server |
| vcs | | Version control software aka git |
| web | frontend | Web server |
| wrk | worker | Worker - server doing batch jobs |

### Disk

| term | code | description |
| --- | --- | --- |
| SSD | ssd | Solid State Disk |
| HDD | hdd | Hard Drive Disk |

### Group
 Universal purpose discrimination designation based on [ATIS phonetic alphabet](https://en.wikipedia.org/wiki/NATO_phonetic_alphabet#Letters).

| term | code |
| --- | --- |
| A | alpha |
| B | bravo |
| C | charlie |
| D | delta |

### Environment
 Three letters describing project environment.

| term | name | description |
| --- | --- | --- |
| dev | Development | for working with other team members |
| orc | Orchestration | for orchestrating environment(s) |
| reg | Registry | for orchestrating environment(s) |
| mon | Monitoring | for orchestrating environment(s) |
| cid | CI & CD | for orchestrating environment(s) |
| pro | Production | for producing bugs and unexpected behavior |
| stg | Staging | for integration with other teams, clients or QA |
| ~~tst~~ | Test | for testing before deploying on production |

### Instance

#### [OVH VPS](https://www.ovh.cz/vps/vps-ssd.xml)
| provider | term | code | CPU | RAM | SIZE | DISK |
| --- | --- | --- | --- | --- | --- | --- |
| OVH | VPS 1 | vps-1 | 1 | 2GB | 10GB | SSD |
| OVH | VPS 2 | vps-2 | 1 | 4GB | 20GB | SSD |
| OVH | VPS 3 | vps-3 | 2 | 8GB | 40GB | SSD |

### Project
 Four letters describing project.

| term | description |
| --- | --- |
| iHub | iHub - integration hub |
| lusk | lusk.io |
| noop | services shared across other projects logs, management etc. |

### Provider
 Three letters describing server provider.

| term | code | name |
| --- | --- | --- |
| AWS | aws | [Amazon Web Services](https://aws.amazon.com/) |
| OVH | ovh | [OVH](https://www.ovh.com/) |

### Specialization

| designation | term | description |
| --- | --- | --- |
| ams | group-bravo-video-interview | Temporary for group B deployment |
| ams | group-bravo-introduction-video | Temporary for group B deployment |
| ams | video-interview | Server for recording video interviews |
| ams | introduction-video | Server for recording introduction videos |

{% extends "../../_templates/environment.md" %}

{% block name %}CI & CD{% endblock %}

{% block services %}

| host:port | name | purpose |
| --- | --- | --- |
| [ci.invire.me:5000](https://ci.invire.me:5000) | CI Docker Registry | Storage for build images |
| [ci.invire.me:9000](https://ci.invire.me:9000) | Staging Docker Registry | Storage for published images that are not supposed to be used in production or staging environment |

{% endblock %}

{% block rancher %}[noop CI & CD](https://ranch.invire.me/env/1a11){% endblock %}

{% block machines %}
{% include "../inventory/ci-cd.md" %}
{% endblock %}

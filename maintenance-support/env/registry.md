{% extends "../../_templates/environment.md" %}

{% block name %}Registry{% endblock %}

{% block services %}

| host:port | name | purpose |
| --- | --- | --- |
| [reg.invire.me:443](https://reg.invire.me) | Production Docker Registry | Storage for build images |

{% endblock %}

{% block rancher %}[noop Registry](https://ranch.invire.me/env/1a12){% endblock %}

{% block machines %}
{% include "../inventory/registry.md" %}
{% endblock %}

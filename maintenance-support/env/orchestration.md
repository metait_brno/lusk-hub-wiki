{% extends "../../_templates/environment.md" %}

{% block name %}Orchestration{% endblock %}

{% block services %}

| host:port | name | purpose |
| --- | --- | --- |
| [ranch.invire.me:443](https://ranch.invire.me) | Production Rancher | Orchestration server |

{% endblock %}

{% block machines %}
{% include "../inventory/orchestration.md" %}
{% endblock %}

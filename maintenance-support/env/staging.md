{% set domain = "video-recruit-beta.com" %}

{% extends "../../_templates/environment.md" %}

{% block name %}Staging{% endblock %}

{% block domain %}{{ domain }}{% endblock %}

{% block rancher %}[iHub STAging](https://ranch.invire.me/env/1a122/){% endblock %}

{% block description %}

For preparing production deployments on identical setup with same base number of machines.  
**It is also used by customers to test their integration.**

{% endblock %}

{% block subdomains %}

### Services

| subdomain | stack | service | description |
| --- | --- | --- | --- |
| [admin](https://admin.{{ domain }}) | app | service | |
| [api](https://api.{{ domain }}) | app | service | |
| [interview](https://interview.{{ domain }}) | web-recorder | server | |
| [logger](https://logger.{{ domain }}) | web-recorder | logger | |
| `none` | frontend | terminator | returns 418 when client tries to reach `{{ domain }}`. |
| `http` | frontend | ssl-redirecter | redirects clients to HTTPS if domain ends with `.{{ domain }}` otherwise return 418 |
| `*` | app | service | partner webs |

### Designations
| subdomain | designation | description |
| --- | --- | --- |
| `ams<counter:2>` | ams | AMS servers for uploading video interview and introduction videos |

{% endblock %}

{% block machines %}
{% include "../inventory/staging.md" %}
{% endblock %}

{% extends "../../_templates/environment.md" %}

{% block name %}Monitoring{% endblock %}

{% block services %}

| host:port | name | purpose |
| --- | --- | --- |
| [log.invire.me:9000](https://log.invire.me:9000) | Sentry | Collects exceptions |
| log.invire.me:5555 (TCP) :5556 (UDP) | Riemann | Processes events like host statistics and custom checks |
| [log.invire.me:4567](http://log.invire.me:4567) | Riemann Dashboard | Displays interesting events from Riemann |
| [graylog.invire.me:9000](https://log.invire.me:9000), [:12900](https://log.invire.me:12900) (REST API) | Graylog | Collects and processes logs and raises alerts |

{% endblock %}

{% block rancher %}[noop Monitoring](https://ranch.invire.me/env/1a9){% endblock %}

{% block machines %}
{% include "../inventory/monitoring.md" %}
{% endblock %}

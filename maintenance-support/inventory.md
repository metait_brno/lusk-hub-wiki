# Inventory
domain: `invire.me`

## Projects

### iHub

#### [Production](env/production.md)

{% include "./inventory/production.md" %}

#### [Staging](env/staging.md)

{% include "./inventory/staging.md" %}

#### [Development](env/development.md)

{% include "./inventory/development.md" %}

### noop

#### [CI & CD](env/ci-cd.md)

{% include "./inventory/ci-cd.md" %}

#### [Monitoring](env/monitoring.md)

{% include "./inventory/monitoring.md" %}

#### [Orchestration](env/orchestration.md)

{% include "./inventory/orchestration.md" %}

#### [Registry](env/registry.md)

{% include "./inventory/registry.md" %}


## Terms

{% include "./terms.md" %}

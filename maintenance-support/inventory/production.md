| name | ip | designation | specialization | group | provider | instance | data center |
| --- | --- | --- | --- | --- | --- | --- | --- |
| palace | 164.132.58.60 | web | | | OVH | VPS 3 | SBG-1 |
| tractor | 164.132.58.6 | web | | | OVH | VPS 3 | SBG-1 |
| ~~cello~~ | 164.132.58.59 | ams | video-interview | | OVH | VPS 1 | SBG-1 |
| fuji | 164.132.58.51 | ams | introduction-video | A | OVH | VPS 1 | SBG-1 |
| robin | 164.132.58.23 | wrk | | | OVH | VPS 1 | SBG-1 |
| husband | 92.222.94.49 | ams | video-interview | A | OVH | VPS 1 | SBG-1 |
| tictac | 164.132.58.59 | ams | group-bravo-introduction-video | B | OVH | VPS 1 | SBG-1 |
| single | 51.254.116.213 | ams | group-bravo-video-interview | B | OVH | VPS 1 | SBG-1 |

| name | ip | designation | specialization | provider | instance | data center |
| --- | --- | --- | --- | --- | --- | --- |
| dexter | 92.222.68.162 | web | | OVH | VPS 3 | SBG-1 |
| budget | 92.222.68.163 | web | | OVH | VPS 3 | SBG-1 |
| shrink | 92.222.68.160 | ams | video-interview | OVH | VPS 1 | SBG-1 |
| pony | 92.222.68.161 | ams | introduction-video | OVH | VPS 1 | SBG-1 |
| mental | 92.222.68.16 | wrk | | OVH | VPS 1 | SBG-1 |

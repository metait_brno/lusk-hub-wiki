| name | ip | designation | specialization | provider | instance | data center |
| --- | --- | --- | --- | --- | --- | --- |
| falcon | 51.254.38.225 | web | | OVH | VPS 2 | SBG-1 |
| tornado | 51.254.38.227 | wrk | | OVH | VPS 1 | SBG-1 |
| vincent | 51.254.38.228 | ams | video-interview | OVH | VPS 1 | SBG-1 |
| avalon | 51.254.123.219 | ams | introduction-video | OVH | VPS 1 | SBG-1 |

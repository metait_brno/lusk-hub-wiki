# Infrastructure

## [Inventory](inventory.md)

List of all servers and terms used to describe them.

## [Environments](environments.md)

List of all environments.

## [DNS](dns.md)

DNS convention explained.

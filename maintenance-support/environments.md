# Environments

## iHub
Environments for testing, preparation for live deployment and live deployment.

* [Development](env/development.md)
* [Staging](env/staging.md)
* [Production](env/production.md)

## noop
Internal projects shared across iHub environments and supporting team.

* [CI & CD](env/ci-cd.md)
* [Monitoring](env/monitoring.md)
* [Orchestration](env/orchestration.md)
* [Registry](env/registry.md)

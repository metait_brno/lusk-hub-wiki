# Add Host

Host should have already set reverse DNS record and hostname randomly chosen from [name pool](../names.txt).

**Append** row with host information to the environment inventory table.

Update DNS records (application and inventory).

```sh
./inventory check-dns <team> <environment>
```

Provision.

```sh
./inventory ansible-playbook <team> <environment> setup.yml -l <host>
```

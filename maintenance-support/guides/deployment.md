# Deployment
Deployments are fully automated. Manual deployments are frowned upon but sometimes necessary for example: when debugging deployment process; performing migrations; testing new deployment scripts.  
Before changing deployment configurations you must become familiar with [all the terms](#terms).  
All deployment specific terms are in *italic* to stress their meaning.

## Organization

Each folder in repository represents a stack.
Definitions of services are in docker-compose.yml and rancher-compose.yml (optional) files located within a stack folder.

## Use-cases

### Adding or updating *service*
1. Checkout *environment* repository

    ```sh
    git clone git@...:<team>/environment-<name>.git
    ```

2. Edit *service* definition

    ```sh
    vim frontend/docker-compose.yml
    ```

3. Commit changes
4. Add new variables (optional)  
see [changing secrets](#changing-secrets-or-settings)
5. Push changes

### Changing secrets or settings
You can define global or stack specific variables.  
Always use UPPER CASE names.  
Stack specific variables are prefixed with `STACK_<NAME>_`. Variables prefixed with `STACK_` are available only for stacks with matching name. They can't be used in other stacks.

1. Go to *environment* repository settings
2. Open Variables page
3. Add, edit or remove variable
4. Save changes
7. Find latest build of master branch
8. Retry

## Limitations
* **DO NOT change definition of DVC** (data loss)
* **DO NOT scale databases** (data loss)

* **Upgrading loadbalancer will cause few seconds outage.**

* Service removed from configuration is not removed from environment. It MUST BE removed manually. (not implemented)
* If loadbalancer configuration is bad it WILL NOT be updated.
* Sidekick CAN NOT be added to already deployed service.
* Sidekick CAN NOT be removed from already deployed service.
* It IS NOT possible to pull image for a sidekick.

## Known Issues
* Some container options are not implemented in Rancher [rancher/rancher #4708](https://github.com/rancher/rancher/issues/4708).
* Container log options are not set [rancher/rancher #4473](https://github.com/rancher/rancher/issues/4773).
* If loadbalancer has not updated it might be because of invalid certificate. Go to Rancher/Infrastructure/Certificates and check (action Edit) if certificate has proper values. Especially check for begin and end block comments.

## Terms

#### [Rancher](http://rancher.com/)
Platform for operating Docker in production.
Its core concepts are [Environments](http://docs.rancher.com/rancher/latest/en/concepts/#environments), [Stacks](http://docs.rancher.com/rancher/latest/en/concepts/#stacks), Services, [Sidekicks](http://docs.rancher.com/rancher/latest/en/concepts/#sidekicks), [Loadbalancer](http://docs.rancher.com/rancher/latest/en/concepts/#load-balancer), [Labels](http://docs.rancher.com/rancher/latest/en/labels/) and [rancher-compose](http://docs.rancher.com/rancher/latest/en/rancher-compose/).

#### Environment
Each environment has own GitLab project with assigned *deployer*. All environment specific variables and secrets are stored as [secure variables](http://docs.gitlab.com/ce/ci/variables/README.html#user-defined-variables-secure-variables) in GitLab project.

#### Deployer
GitLab CI runner with [deployment scripts](https://gitlab.video-recruit.com/invire/docker-gitlab-ci-rancher-deployer/blob/master/deploy) authorized to make changes to the *environment*.

#### Secrets
Are GitLab projects secure variables.

#### DVC Data Volume Container
Container (usually a *sidekick*) that stores data and is only started once.

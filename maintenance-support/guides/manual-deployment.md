# Manual Deployment

Please read [deployment page](./deployment.md) first.

* Setup

  ```sh
  git clone invire/docker-gitlab-ci-rancher-deployer
  git clone ihub/environment-dev
  cd environment-dev
  ln -s ../docker-gitlab-ci-rancher-deployer/deploy
  ln -s ../docker-gitlab-ci-rancher-deployer/secrets
  ```

* Generate API credentials `Rancher > API > Add Environment API Key`

  ```sh
  export RANCHER_URL=https://ranch.invire.me/v1/projects/<ENVIRONMENT-ID>
  export RANCHER_ACCESS_KEY=<ACCESS-KEY>
  export RANCHER_SECRET_KEY=<SECRET-KEY>
  ```

* Export GitLab project secrets as environment variables  
  You will find `PRIVATE-TOKEN` in `GitLab > Profile > Account > Private Token`

  ```sh
  eval "$(PRIVATE_TOKEN=<PRIVATE-TOKEN> ./secrets ihub/environment-dev | sed -e 's/^.\+=/export \0/g' )"
  ```

* Deploy environment, stack or a single service

  ```sh
  ./deploy [<stack> [<service>]]
  ```

# Remove Host

**Cross out host name** in the environment inventory table.

| name | ... |
| --- | --- |
| ... | ... |
| ~~foo~~ | ... |
| ... | ... |

**Before proceeding double check that host is not running any mission critical containers like databases, scale of 1 services and others.**

1. Disable or remove configuration from all services pointing to the host.
2. Remove application DNS records pointing to host.
3. Deactivate host in Rancher.
4. Delete host.
6. Purge host in Rancher.
5. Remove inventory DNS records.

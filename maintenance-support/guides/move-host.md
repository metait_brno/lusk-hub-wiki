# Moving Host

**Only when absolutely necessary use unwiped old host.**  
If possible [remove old](remove-host.md) and [add new one](add-host.md).  
*Instead of deleting old one and creating new one you can reinstall host image.*

## Remove Host from Old Environment

1. **Double check that host is not running any mission critical containers like databases, scale of 1 services and others.**
2. Disable or remove configuration from all services pointing to the host.
3. Remove application DNS records pointing to host.
4. Go to Rancher Environment > Infrastructure
5. From menu for the host choose Deactivate.
6. After host deactivates choose purge.


### Update environments inventory tables

**Cross out** host name in the old environment inventory table.

| name | ... |
| --- | --- |
| ~~foo~~ | ... |

**Append the row** to the new environment inventory table.

| name | ... |
| --- | --- |
| ... | ... |
| foo | ... |


Remove all docker images and running containers from host.

`<host>` is the new hostname randomly chosen from [name pool](../names.txt).  
`<fqdn>` is the new FQDN of the host.

Update inventory DNS records.

```sh
docker-compose run --rm x check-dns <team> <env>
```

## Provision Host

Remove all docker images.
```sh
docker rmi $(docker images -q)
```

Remove rancher status directory.
```sh
ssh <fqdn> rm -r /var/lib/rancher
```

Playbook will update the hostname and add host to new Rancher environment.   *Server restart might be necessary for some system services to work properly.*

```sh
./inventory ansible-playbook <team> <environment> setup.yml -l <host>
```


```hosts
# ...
<ip> <fqdn> # ...
```

Update application DNS records.

```sh
./inventory check-dns <team> <environment>
```

# Guides

## [Deployment](guides/deployment.md)

Guide on how to deploy new versions of services to dev.

## [Manual Deployment](guides/manual-deployment.md)

Guide on how to deploy new versions of services to staging or production.

## [Add Host](guides/add-host.md)

How to add host (physical server, VM or VPS) to environment.

## [Move Host](guides/move-host.md)

How to move host (physical server, VM or VPS) between environments.

## [Remove Host](guides/remove-host.md)

How to remove host (physical server, VM or VPS) from environment.

# Live Interview Definition

10\. 2. 2016

---

Live interview is being developed as a new HR tool integrated withing the Integration Hub platform. It will offer recruiters to invite candidate for the online interview between 1 recruiter and 1 candidate.


## Main features

- access roles split to **candidate** (1), **recruiter** (2) and **viewers** (x)
- live interview fill be usable just for partners with **activated** `liveInterview` feature
- a **meet date** and **time** is defined
- invitation will be accesible by combination of the unique "**room hash**" and **role-specific hash** (candidate/recruiter/viewer)
- invitation will contain title, short description and one language will be selected as a default language for communication
- invitation will be assigned to 1 **candidate** and 1 **recruiter** - author will be predefined as the assigned recruiter but can be changed to any other recruiter within the system
- invitation has some states defined: *pending, declined, confirmed (by candidate), canceled, recorded, completed, active*
- public url exists to be send to viewers
- there're **session timeline notes** for each invitation written by the recruiter (session datetime is stored to each note next to the server time)
- there's a **chat history between recruiter and viewers** (name, role, text, session time are stored)
- there's a **chat history between recruiter and candidate** (name, role, text, session time are stored)
- there's a **discussion (comment)** above each interview (comments between recruiter within the hub interface)
- there're also another private fields stored for the internal purposes:
	- author
	- create date
	- recruiter join date
	- candidate join date (has to be later than recruiter join date)
	- session start date - starts once a recruiter joins
	- session end date (on recruiters action)
	- video session start date (on recruiters action)
	- video session end date (on recruiters action)
	- reminder date
	- decline date
	- decline reason
	- cancel date
	- cancel reason
	- confirmed date
	- completed date
- communication around the whole process will be covered by many email templates, see below


## Invitation state conditions
- **pending/confirmed** invitation allows to recruiter to change just **meet date** (this action sends rescheduled notification), **cancel** invitation, it's possible to be **declined** by candidate
- once an invitation is **rescheduled**, it's state changes to **pending** and a new confirmation notification is on the way to candidate/recruiter
- it's **not** possible to cancel/decline started interview
- if interview is not started after 30 minutes after the meet date, invitation is **canceled** because of missing recruiter
- once an invitation session started, it's state changes to `active`


## Email message
- invitation for recruiter and candidate (2 different messages)
- reminder for recruiter and candidate (2 different messages)
- declined notification for recruiter and candidate (2 different messages)
- recorded notification for recruiter and candidate (2 different messages)
- completed notification for recruiter
- canceled notification for recruiter and candidate (2 different messages)
- rescheduled notification for recruiter and candidate (2 different messages)
- expired notification for recruiter and candidate (with reason that recruiter not join)
# Automated Question Suggestion

8\. 7. 2016

---

At this moment, Hub platform offers to the clients to predefine their own library of questions and sets to be re-used for video interview invitations.

We also offer predefined list of questions and sets, which are cloned regularly to the client's account at the moment when the client's accounts are created.

**Automated Question Suggestion** (called `AQS`) is taking place to offer a new way, how questions can be used for the invitations. The principle of the AQS is to provide a functionality to generate **random list of questions** for suggested job position. These questions are predefined and translated on our side to multiple languages used in the iHub platform.

## Demo

If you want to see a preview draft version of the AQS, visit this [demo page](http://vr.iglazz.cz/questions-test/).


## How it works?

Imagine the situation: You want to invite some candidates for an interview for a job position. Let's assume the position title is *PHP programmer* and our invitation language is *English*. 

Question suggestion tries to find this job position in the list of predefined position **aliases**, and **create a result** with the list of suggested questions. This suggestion is based on the position definition.

Position definitions are used to filter *English* questions by category and expertise. If multiple questions exist for the same combination of the category/expertise, one question is selected randomly - but whole question list result has to contain an unique list of questions - no duplicities. If there's no question available for the definition, than it's skipped.


## Used Terms

### Category

Category is the primary identifier of the question for the job position. Each category holds the priority value, which is then used for question sorting.

### Expertise

Expertise is the secondary identifier of the question for the job position. It's more precious than the category.

### Position

Position is the core group, which holds the definition of questions used for suggestion. It's identified by a unique name, which is not the real name of the position - it's more like a position type.

Each position contains (next to the title) a list of aliases as well as a list of definitions.

### Position alias

Alias is the real representation of the job position. It's defined for the position and language, and it's combination of title and language has to be unique.

### Position definition

Defines the category and the expertise for the position. Multiple categories/expertises are allowed for one position.

### Question

AQS question is similar to IHub's Library question - it contains text and duration used for the interview recording. The only difference between them is that an AQS question will be translated to the different languages. And therefore it should contain the text directly, but should contain the relation for the question translation.

### Question translation

Every question has to be translated into multiple languages. Since the definition of the question is same for all translations, it's stored in the question entity. Translation contains just information specific for the translation.
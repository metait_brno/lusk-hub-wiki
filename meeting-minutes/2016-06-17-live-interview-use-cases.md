# Live Interview - Use Cases

This document describes special scenarios that may occur during the Live Interview to the **Recruiter** or the **Candidate**.

> Whenever a person is disconnected from the server, frontend app's trying to reconnect. In this case, an overlay is displayed to the disconnected user with information about reconnecting.

Let assume we're having a valid invitation, and recruiter, candidate and 1 viewer connected.


## 1. Interview has been confirmed

When interview is still in the **confirmed** state (means the session and the recording never started), it's not the issue. In this state, recruiter's still **waiting** for the candidate and is not able to start the session until he's connected.

It affects the candidate in the same way - candidate is still **waiting** for the room session to be enabled.

Also connected viewer is still **waiting** for the room session to be enabled.


## 2. Interview session was started

When interview session has been already **started**, the complexity of the solution is much more bigger. We have to take care of the interview when both candidate and recruiter are disconnected.

For this case, when candidate or recruiter is/are disconnected, an **automated close operation is managed** (enabled for the interview).

> This automated script is searching for the interviews without candidate/recruiter with autoclose option enabled, and automatically closes the interviews after 10 minutes.


### 2.1. Candidate disconnected

#### How it affects Recruiter

All components related to the candidate have to be **disabled** - it's not possible to chat with candidate, write session notes, to see candidate's video preview anymore. Also start recording button have to be disabled. It's possible to close the interview manually.

If candidate is not connected in 5 minutes after he was disconnected, frontend application shows the notice to the recruiter with message, that candidate is still trying to connect to the interview and probably have some issues. It's possible to close the interview manually from this component view, and also turn off the automated script.

#### How it affects viewer

Video preview component of the candidate has to be disabled.


### 2.2. Recruiter disconnected

When recruiter is disconnected, **automated close script has to be enabled again** for the interview.

#### How it affects candidate

When a recruiter is disconnected during the started interview, all components related to the recruiter have to be **disabled** - it's not possible to chat with recruiter, to see the recruiter's preview anymore. A new notification is shown to the candidate informing about recruiter's having these issues.

After **5 minutes** it shows the information about continuous problems with recruiter's connection and informing about automated script which will close the interview automatically after **next 5 minutes**.

#### How it affects viewer

Video preview component of the recruiter has to be disabled as well as the chat component between recruiter and viewer.


## 3. Interview session started + recording started

When interview **recording has been already started**, we have to check and ensure both recruiter's and candidate's streams are **stopped** on AMS server.

### 3.1. Candidate disconnected

#### How it affects Recruiter

All components related to the candidate have to be **disabled** - it's not possible to chat with candidate, write session notes, to see candidate's video preview anymore. Stop recording button have to change to "Continue recording" button - it allows recruiter to start recording again and append new video stream to the previously recorded one. It's possible to close the interview manually.

If candidate is not connected in 5 minutes after he was disconnected, frontend application shows the notice to the recruiter with message, that candidate is still trying to connect to the interview and probably have some issues. It's possible to close the interview manually from this component view, and also turn off the automated script.

#### How it affects viewer

Video preview component of the candidate has to be disabled.


### 3.2. Recruiter disconnected

When recruiter is disconnected, **automated close script has to be enabled again** for the interview.

#### How it affects Candidate

All components related to the recruiter have to be **disabled** - it's not possible to chat with recruiter and see recruiter's video preview anymore.

After 5 minutes it shows the information about continuous problems with recruiter's connection and informing about automated script closing the interview automatically after 5 minutes.

#### How it affects viewer

Video preview component of the recruiter has to be disabled and it's not possible to chat with the recruiter anymore.


## 4. Interview has been recorded

### 4.1. Candidate disconnected

When interview is already **recorded**, candidate is automatically disconnected from the server. It's not possible to connect to the interview since it's closed from the candidate's perspective.

#### How it affects Recruiter

Nothing special happen to the recruiter's UI, because it's **expected** behaviour. Candidate is not connected when interview was recorded. Only recruiter and viewers are able to connect the interview with this state.

#### How it affects viewer

Nothing special happen to the viewer's UI, because it's **expected** behaviour. Candidate's video preview is disabled.

### 4.2. Recruiter disconnected

When recruiter is disconnected, **automated close script has to be enabled again** for the interview.

#### How it affects Candidate

> Nothing happens to the candidate's because it's not present in the application at this moment.

#### How it affects viewer

Video preview component of the recruiter has to be **disabled** and it's not possible to chat with the recruiter anymore.

## 5. Interview has been closed

Whenever interviews is closed, it's **not allowed to anybody** to connect to the interview. All persons are notified with the respective message "Interview has already passed.".
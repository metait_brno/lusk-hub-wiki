# About the iHub Team

You can find important contacts, of all members of the Integration Hub Team.

| Name | Mail | Phone | Skype | Github Profile | LinkedIn Profile |
| -- | -- | -- | -- | -- | -- |
| Jirka | [![mail](../_assets/ico-mail.png)](mailto:jiri.petvaldsky@video-recruit.com) | 774 303 066 | - | [![mail](../_assets/ico-github.png)](https://github.com/jpetvaldsky) | [![mail](../_assets/ico-linkedin.png)](http://www.linkedin.com/in/petvaldskyjiri) |
| Lukáš | [![mail](../_assets/ico-mail.png)](mailto:lukas.tomek@video-recruit.com) | 723 148 144 | stene-cz | [![mail](../_assets/ico-github.png)](https://github.com/Stene3) | [![mail](../_assets/ico-linkedin.png)](https://cz.linkedin.com/in/lukastomek) |
| Pepa | [![mail](../_assets/ico-mail.png)](mailto:josef.koren@video-recruit.com) | 608 643 542 | - | [![mail](../_assets/ico-github.png)](https://github.com/pepoy) | - |
| Petr | [![mail](../_assets/ico-mail.png)](mailto:petr.vnenk@video-recruit.com) | 721 636 123 | - | [![mail](../_assets/ico-github.png)](https://github.com/vnenkpet) | [![mail](../_assets/ico-linkedin.png)](https://www.linkedin.com/in/petrvnenk) |
| Michal | [![mail](../_assets/ico-mail.png)](mailto:michal.gebauer@video-recruit.com) | 774 647 425 | live:mishak | [![mail](../_assets/ico-github.png)](https://github.com/mishak87) | [![mail](../_assets/ico-linkedin.png)](https://cz.linkedin.com/in/michalgebauer) |

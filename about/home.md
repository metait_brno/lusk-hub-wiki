# About Integration Hub

Integration Hub Team is responsible for the development of the Integration Hub platform - application developed primary as a set of tools, which can be integrated to any of the Applicants Tracking System.


## Why Integration?

It's simple. We think that the company can't concentrate just on e2e platforms and has to be able to be a part of another HR systems.

We have rich experience with Integration in Czech Republic, and we concentrate on other integrations all over the world.

If you want to read more about the Product, what is offers, how it works and much more, follow to the [About the Product](./product.md) chapter.


## Who?

We are a small team. But everybody has a rich history full of various projects for various clients and their size. Do you still want to know more about the team? [Chapter dedicated to the team](./team.md) is the right place for you!


## How?

It's not the right place. Try to find more information within our [guidelines](../guidelines/home.md).
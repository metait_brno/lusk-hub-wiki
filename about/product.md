# About the Product

Integration Hub platform is developed since *May 2015*, when the decision came to split development into 2 teams: one responsible for development of the [own ATS solution](http://lusk.io) and the second team responsible for the integrations and providing HR tools.


## Currently available HR Tools

At this moment, Integration Hub (shorten to iHub) offers just one tool usable for HR companies - **video interviews**. It allows to send invitations directly to customer candidates, and record video answers for customer predefined questions.

But we work hard to add other tools, such as live interview solution, or coding tests and more in future.


## Cool Integrability

Integration Hub is not there for direct sales. It's power consists in a simple idea to let any existing ATS system be integrated with our solution. It's applicable in a very fast way, dependently on the scope.

### Simple Integration via Hub Interface

The biggest advantage of this variant consists in very simple integration. From partner's side it's just about providing of some needed information, like candidate's info, and user's information from the platform, where is he currently signed in.

Once these data are provided, user is able to enter iHub Interface, which is running on our servers under our domain (it lies on the partner-specific sub-domain), which is highly customized to look and feel like an ATS application, where it's integrated to.

### Full integration via API

If our integration partner decides to not let user to be forwarded into the iHub Interface, all functionality can be developed over the REST API. Almost every single action can be performed over the API and integration partner is able to fetch all necessary information from the Ihub API into their ATS platform.

To keep it simple as much as possible, we have very [cool documentation](http://docs.ihubintegration.apiary.io). It contains description for all functionalities we offer, and also a mock server to quickly preview the API.


---
### What next?

- [About the Team](./team.md)
# {% block wip %}{% endblock %} Milestone v{% block release %}0.1{% endblock %}

*Due date: {% block date %}{% endblock %}*

---

## Features and Improvements

{% block tasks %}
> There are no new features during the milestone.
{% endblock %}


## Past Release Fixes

{% block fixes %}
> There are no planned fixes during the milestone.
{% endblock %}


## What next?

- [List of all milestones](./home.md)
## {% block headline %}{% endblock %}
*Release date: {% block date %}{% endblock %}*

{% block content %}
{% endblock %}
